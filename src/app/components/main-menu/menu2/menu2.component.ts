import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../models/menu.model';
import {MenuService} from '../../../services/menu.service';

@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.scss']
})
export class Menu2Component implements OnInit {
route: MenuModel[];

  constructor(private menuService: MenuService) {
  }

  ngOnInit(): void {
    this.route = this.menuService.allRoutes;
  }

}
