import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../models/menu.model';
import {MenuService} from '../../../services/menu.service';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {

  route: MenuModel[];

  constructor(private menuService: MenuService) {
  }

  ngOnInit(): void {
    this.route = this.menuService.allRoutes;
  }

}
