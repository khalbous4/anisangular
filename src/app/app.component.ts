import {Component} from '@angular/core';
import {MenuModel} from './models/menu.model';
import {MenuService} from './services/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  title = 'Anis1angular';
  route: MenuModel[] = this.menuService.allRoutes;
  constructor( private menuService: MenuService) {

  }
}
