import { Injectable } from '@angular/core';
import {MenuModel} from '../models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  allRoutes: MenuModel[] = [{url: 'home', name: 'accueil', age: 0}, {url: '-à-propos', name: 'à propos', age: 30}];


  constructor() { }
}
